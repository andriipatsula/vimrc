# README #

vim's configuration includes several plugins, configurations and mapping. Added additional highlights for C++, Python, C# and Rust languages.

# Used plugins #

* VundleVim/Vundle.vim
* altercation/vim-colors-solarized
* flazz/vim-colorschemes
* joshdick/onedark.vim
* joshdick/airline-onedark.vim
* bling/vim-airline
* kien/ctrlp.vim
* majutsushi/tagbar
* Shougo/unite.vim
* Shougo/vimfiler.vim
* rust-lang/rust.vim
* octol/vim-cpp-enhanced-highlight
* hdima/python-syntax
* OrangeT/vim-csharp.git
* Valloric/YouCompleteMe
* MattesGroeger/vim-bookmarks
* tomtom/tcomment_vim